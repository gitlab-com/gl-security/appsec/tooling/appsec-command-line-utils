function test-pat() {
  for host in gitlab.com staging.gitlab.com dev.gitlab.org ops.gitlab.net staging-ref.gitlab.com release.gitlab.net pre.gitlab.com; do
    echo Testing $host
    curl --header "Private-Token: $1" "https://$host/api/v4/user"
    echo
  done
}

export inventory_dir=/tmp/inventoryqueries
function inventory-check() {
  name=$1

  if [ -d "$inventory_dir" ]; then
    git -C "$inventory_dir" pull --quiet --depth 1 --no-rebase
  else
    git clone --quiet --depth 1 git@gitlab.com:gitlab-com/gl-security/product-security/inventory.git "$inventory_dir"
  fi

  7z x "$inventory_dir/db/inventory.db.7z" "-o$inventory_dir" -aoa >/dev/null
  inventory-query-dependency "$name"
}

function inventory-query-dependency() {
  name=$1
  sqlite3 -markdown "$inventory_dir/db/inventory.db" "select d.name, d.version, d.dependency_file_path, p.name, p.web_url from dependencies d join projects p on d.project_id = p.id where d.name LIKE '$name';"
}

function update-appsec-utils() {
  # https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
  git -C $SCRIPT_DIR pull
}

function diffgem() {
  gemname="$1"
  version1="$2"
  version2="$3"
  gem install "$gemname" -v "$version1" --install-dir "/tmp/$gemname-$version1" --no-verbose
  gem install "$gemname" -v "$version2" --install-dir "/tmp/$gemname-$version2" --no-verbose
  mkdir "/tmp/$gemname-diffs" &&  pushd "/tmp/$gemname-diffs"
  mv /tmp/$gemname-$version1/gems/$gemname-$version1*/{.[!.]*,*} "/tmp/$gemname-diffs/"
  git init && git add . && git commit -m "$gemname-$version1"
  find "/tmp/$gemname-diffs/" -mindepth 1 -maxdepth 1 -not -name .git -exec rm -rf {} \;
  mv /tmp/$gemname-$version2/gems/$gemname-$version2*/{.[!.]*,*} "/tmp/$gemname-diffs/"
  git diff '**/*.rb' ':!spec'
  popd
}
